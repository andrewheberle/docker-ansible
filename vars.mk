# Image config
ANSIBLE_VERSION = 2.8.4
BASE_IMAGE = registry.gitlab.com/andrewheberle/docker-base
BASE_TAG = 3.10

# Registry path for tagging image
REGISTRY = registry.gitlab.com/andrewheberle

# Image information/metadata
IMAGE_ARCH = amd64
IMAGE_AUTHOR = Andrew Heberle
IMAGE_DESCRIPTION = Ansible Controller
IMAGE_NAME = docker-ansible
IMAGE_VCS_BASE_URL = gitlab.com/andrewheberle

# Values to replace in the Dockerfile
DOCKERFILE_REPLACE_VARS = ANSIBLE_VERSION BASE_IMAGE BASE_TAG IMAGE_ARCH IMAGE_AUTHOR IMAGE_DESCRIPTION IMAGE_NAME IMAGE_VCS_BASE_URL
